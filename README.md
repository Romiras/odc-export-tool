An export tool for BlackBox (Oberon/F) compound documents, ODC.

Export to:
# XHTML (UTF-8)
# DokuWiki markup (UTF-8)
# Plain text (UTF-8)


ArcExportDoc.Do traverses a BlackBox root tree for ODC documents and exports them to XHTML in root directory in batch mode.

Dependencies:
# Library FreeImage
# BlackBox Component Freeimage. See FreeImage at https://bitbucket.org/Romiras/bindings
